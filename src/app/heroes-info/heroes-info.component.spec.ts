import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesInfoComponent } from './heroes-info.component';

describe('HeroesInfoComponent', () => {
  let component: HeroesInfoComponent;
  let fixture: ComponentFixture<HeroesInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
