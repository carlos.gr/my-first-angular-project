import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../heroes';
import { HeroModelService } from '../hero-model.service';
import { MessageService } from '../message.service';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-heroes-info',
  templateUrl: './heroes-info.component.html',
  styleUrls: ['./heroes-info.component.css']
})
export class HeroesInfoComponent implements OnInit {
	hero: Hero; 

	constructor(
		private route: ActivatedRoute,
		private heroesService: HeroModelService,
		private location: Location
	) {}

	ngOnInit(): void {
		this.getHeroInfo();
	}

	getHeroInfo(): void {
	  const id = +this.route.snapshot.paramMap.get('id');
	  this.heroesService.getHeroeInfo(id).subscribe((hero:Hero) => this.hero = hero);
	}

	goBack(): void {
	    this.location.back();
	}

	save(): void {
	   this.heroesService.updateHero(this.hero).subscribe(() => this.goBack());
	}

}
