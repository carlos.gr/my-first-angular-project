import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()
export class InMemoryDataService implements InMemoryDataService{

  constructor() { }

	createDb(){
		const heroes = [
			{ id: 1, name: 'Hulk'},
			{ id: 2, name: 'Antman'},
			{ id: 3, name: 'Daredevil'},
			{ id: 4, name: 'Loki'},
			{ id: 5, name: 'Spiderman'},
			{ id: 6, name: 'Captain America'},
			{ id: 7, name: 'Deadpool'},
			{ id: 8, name: 'Dr Strange'},
			{ id: 9, name: 'Hawkeye'},
			{ id: 10, name: 'Iron Fist'}
		];
		return {heroes};
	}

}