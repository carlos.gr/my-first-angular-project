import { Injectable } from '@angular/core';
import { Hero } from './heroes';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap} from 'rxjs/operators';
import 'rxjs/add/operator/map';

import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class HeroModelService {

	private heroesUrl = 'api/heroes';

  	constructor(private http:HttpClient,
  				private messageService: MessageService) { 
  	}

  	/* GET busqueda de heroe dependiendo de lo escrito en el campo ##searchBox */
  	/* hero-search.component.ts */
	searchHeroes(term: string): Observable<Hero[]> {
	  if (!term.trim()) {
	    // if not search term, return empty hero array.
	    return of([]);
	  }
	  return this.http.get<Hero[]>(`api/heroes/?name=${term}`).pipe(
	    tap(_ => this.log(`found heroes matching "${term}"`)),
	    catchError(this.handleError<Hero[]>('searchHeroes', []))
	  );
	}

	/* GET obtiene lista de heroes */
	/* heroes-list.component.ts / heroes-dashboard.component.ts */
	getHeroes (): Observable<Hero[]> {
	  return this.http.get<Hero[]>(this.heroesUrl)
	    .pipe(
	    	tap(heroes => this.log(`fetched heroes`)),
	  	    catchError(this.handleError('getHeroes', []))
	    );
	}

	/* GET obtiene info de heroe dependiendo del id */
	/* heroes-info.component.ts */
	getHeroeInfo (id:number): Observable<Hero> {
	    const url = `${this.heroesUrl}/${id}`;
	    return this.http.get<Hero>(url)
		 .pipe(
		 	tap(_ => this.log(`fetched hero id=${id}`)),
	  	    catchError(this.handleError<Hero>(`getHero id=${id}`))
	     );
	}
 
 	/* PUT actualiza valor de heroe  */
 	/* heroes-info.component.ts */
	updateHero (hero: Hero): Observable<any> {
	  return this.http.put(this.heroesUrl, hero, httpOptions).pipe(
	    tap(_ => this.log(`updated hero id=${hero.id}`)),
	    catchError(this.handleError<any>('updateHero'))
	  );
	}

	/* POST: Agrega nuevo heroe(campo en heroes-list html) */
	/* heroes-list.component.ts */
	addHero (hero: Hero): Observable<Hero> {
	  return this.http.post<Hero>(this.heroesUrl, hero, httpOptions).pipe(
	    tap((hero: Hero) => this.log(`added hero w/ id=${hero.id}`)),
	    catchError(this.handleError<Hero>('addHero'))
	  );
	}	

	/* DELETE elimina un heroes de la lista */
	/* heroes-list.component.ts */
	deleteHero (hero: Hero | number): Observable<Hero> {
	  const id = typeof hero === 'number' ? hero : hero.id;
	  const url = `${this.heroesUrl}/${id}`;

	  return this.http.delete<Hero>(url, httpOptions).pipe(
	    tap(_ => this.log(`deleted hero id=${id}`)),
	    catchError(this.handleError<Hero>('deleteHero'))
	  );
	}

	/* Manejador de errores, recibe nombre de la operacion fallida, result valor opcional para regresar observable */
	private handleError<T> (operation = 'operation', result?: T) {
	    return (error: any): Observable<T> => {
	 
	      // TODO: send the error to remote logging infrastructure
	      console.error(error); // log to console instead
	 
	      // TODO: better job of transforming error for user consumption
	      console.log(`${operation} failed: ${error.message}`);
	 
	      // Let the app keep running by returning an empty result.
	      return of(result as T);
	    };
	}

	/* Log agrega mensaje a mostrarse en pantalla */
	private log(message: string) {
	  	this.messageService.add('HeroService: ' + message, new Date());
	}

}
