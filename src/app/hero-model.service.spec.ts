import { TestBed, inject } from '@angular/core/testing';

import { HeroModelService } from './hero-model.service';

describe('HeroModelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroModelService]
    });
  });

  it('should be created', inject([HeroModelService], (service: HeroModelService) => {
    expect(service).toBeTruthy();
  }));
});
