import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeroesInfoComponent }      from './heroes-info/heroes-info.component';
import { HeroesDashboardComponent }      from './heroes-dashboard/heroes-dashboard.component';
import { HeroesListComponent }      from './heroes-list/heroes-list.component';
import { MessagesComponent } from './messages/messages.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: HeroesDashboardComponent },
  { path: 'heroes', component: HeroesListComponent },
  { path: 'history', component: MessagesComponent },
  { path: 'info/:id', component: HeroesInfoComponent }
];

@NgModule({
  imports: [
	  RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}




