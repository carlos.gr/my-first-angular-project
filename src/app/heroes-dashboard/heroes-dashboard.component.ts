import { Component, OnInit } from '@angular/core';
import { Hero } from '../heroes';
import { HeroModelService } from '../hero-model.service';

@Component({
  selector: 'app-heroes-dashboard',
  templateUrl: './heroes-dashboard.component.html',
  styleUrls: ['./heroes-dashboard.component.css']
})
export class HeroesDashboardComponent implements OnInit {

	topHeroesInfo: Hero[] = [];
  topHeroesImg = [];

	constructor(private httpHeroesService:HeroModelService) { }

	ngOnInit() {
		this.httpHeroesService.getHeroes()
  		.subscribe((heroes: Hero[]) => {
        this.topHeroesInfo = heroes.slice(0,4);
        this.topHeroesImg = this.topHeroesInfo.map(heroes => {heroes.image = heroes.name.toLowerCase().replace(" ", "_");return heroes});
        return this.topHeroesImg;
      });
	}
}
