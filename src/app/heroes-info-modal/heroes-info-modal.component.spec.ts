import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesInfoModalComponent } from './heroes-info-modal.component';

describe('HeroesInfoModalComponent', () => {
  let component: HeroesInfoModalComponent;
  let fixture: ComponentFixture<HeroesInfoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
