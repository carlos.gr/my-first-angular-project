import { Component, OnInit, IterableDiffers } from '@angular/core';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
	notification: string = "";
	iterableDiff;

    constructor(public messageService: MessageService,
				private _iterableDiffers: IterableDiffers
  	){ 
	  this.iterableDiff = this._iterableDiffers.find([]).create(null);
	}

  	ngOnInit() {
  	}

	ngDoCheck() {
	    let changes = this.iterableDiff.diff(this.messageService.messages);
	    let lastMessage = this.messageService.messages[this.messageService.messages.length-1];
	    if (changes && lastMessage != undefined) {
	    	this.notification = '<span class="notify">'+ lastMessage +'</span>';
	    }
	}

}
