import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { HeroModelService } from './hero-model.service';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { HeroesInfoComponent } from './heroes-info/heroes-info.component';
import { HeroesListComponent } from './heroes-list/heroes-list.component';
import { HeroesDashboardComponent } from './heroes-dashboard/heroes-dashboard.component';
import { AppRoutingModule } from './/app-routing.module';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { HeroesInfoModalComponent } from './heroes-info-modal/heroes-info-modal.component';

@NgModule({
  /*Componentes*/
  declarations: [
    AppComponent,
    HeroesInfoComponent,
    HeroesListComponent,
    HeroesDashboardComponent,
    MessagesComponent,
    HeroSearchComponent,
    NotificationsComponent,
    HeroesInfoModalComponent
  ],
  /*Modulos*/
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    HttpClientModule,
    AppRoutingModule
  ],
  /*Servicios*/
  providers: [HeroModelService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
