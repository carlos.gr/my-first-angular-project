import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/observable/of';

@Injectable()
export class MessageService {
  messages: string[] = [];
  history = [];

  	add(message: string, date: Date) {
    	this.messages.push(message);
    	this.history.push({"message":message,"time":date});
  	}

  	clear() {
    	this.messages = [];
    	this.history = [];
  	}
}