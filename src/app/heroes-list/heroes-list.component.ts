import { Component, OnInit } from '@angular/core';
import { Hero } from '../heroes';
import { HeroModelService } from '../hero-model.service';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css']
})
export class HeroesListComponent implements OnInit {

	heroes: Hero[] = [];

	constructor(private heroesService: HeroModelService) { }

	ngOnInit() {
		this.getHeroes();
	}

	getHeroes(): void {
		this.heroesService.getHeroes().subscribe((value: Hero[]) => this.heroes = value);
	}

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    /* { name } as Hero  ==  <Hero>{ name } */
    this.heroesService.addHero(<Hero>{ name }) 
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(heroes => heroes !== hero);
    this.heroesService.deleteHero(hero).subscribe();
  }

}
